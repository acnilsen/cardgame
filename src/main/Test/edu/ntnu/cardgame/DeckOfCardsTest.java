package edu.ntnu.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {
    @Test
    @DisplayName("A new deck of cards should have 52 cards")
    void testNewDeckHas52Cards() {
        // Arrange
        DeckOfCards deck = new DeckOfCards();
        // Act
        int deckLength = deck.getDeck().length;
        // Assert
        assertEquals(52, deckLength);
    }

    @Test
    @DisplayName("Test that the image url is generated correctly")
    void testPositiveDealHand() {
        // Arrange
        DeckOfCards deck = new DeckOfCards();
        // Act
        PlayingCard[] hand = deck.dealHand(5);
        // Assert
        assertEquals(5, hand.length);
    }

    @Test
    @DisplayName("Negative test of dealHand")
    void testNegativeDealHand() {
        // Arrange
        DeckOfCards deck = new DeckOfCards();
        // Act and assert
        assertThrows(IllegalArgumentException.class, () -> deck.dealHand(53));
    }
}