package edu.ntnu.cardgame;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CheckHandTest {

    @Nested
    class PositiveTests {

        @Test
        @DisplayName("Test that a hand with 5 hearts is detected as a flush")
        void testPositiveIsFlush() {
            // Arrange
            PlayingCard[] hand = {
                new PlayingCard('H', 1),
                new PlayingCard('H', 2),
                new PlayingCard('H', 3),
                new PlayingCard('H', 4),
                new PlayingCard('H', 5)
            };
            // Act
            boolean isFlush = CheckHand.isFlush(hand);
            // Assert
            assert(isFlush);
        }

        @Test
        @DisplayName("Test flush on hand with more than five cards")
        void testPositiveIsFlushMoreThanFiveCards() {
            // Arrange
            PlayingCard[] hand = {
                new PlayingCard('H', 1),
                new PlayingCard('S', 12),
                new PlayingCard('S', 7),
                new PlayingCard('H', 4),
                new PlayingCard('C', 9),
                new PlayingCard('S', 3),
                new PlayingCard('S', 9),
                new PlayingCard('S', 10),
                new PlayingCard('S', 6)
            };
            // Act
            boolean isFlush = CheckHand.isFlush(hand);
            // Assert
            assert(isFlush);
        }

        @Test
        @DisplayName("Test that the value of a hand is calculated correctly")
        void testPositiveCardsValue() {
            // Arrange
            PlayingCard[] hand = {
                new PlayingCard('H', 1),
                new PlayingCard('H', 2),
                new PlayingCard('H', 3),
                new PlayingCard('H', 4),
                new PlayingCard('H', 5)
            };
            // Act
            int value = CheckHand.cardsValue(hand);
            // Assert
            assertEquals(15, value);
        }

        @Test
        @DisplayName("Test that the hearts on a hand is returned correctly")
        void testPositiveHeartsOnHand() {
            // Arrange
            PlayingCard[] hand = {
                    new PlayingCard('H', 1),
                    new PlayingCard('S', 12),
                    new PlayingCard('S', 7),
                    new PlayingCard('H', 4),
                    new PlayingCard('C', 9),
                    new PlayingCard('S', 3),
                    new PlayingCard('H', 9),
                    new PlayingCard('S', 10),
                    new PlayingCard('H', 6)
            };
            // Act
            String hearts = CheckHand.heartsOnHand(hand);
            // Assert
            assertEquals("H1, H4, H9, H6", hearts);
        }
    }


    @Nested
    class NegativeTests {

        @Test
        @DisplayName("Test that a hand with 4 hearts and 1 spade is not detected as a flush")
        void testNegativeIsFlush() {
            // Arrange
            PlayingCard[] hand = {
                new PlayingCard('H', 1),
                new PlayingCard('H', 2),
                new PlayingCard('H', 3),
                new PlayingCard('H', 4),
                new PlayingCard('S', 5)
            };
            // Act
            boolean isFlush = CheckHand.isFlush(hand);
            // Assert
            assert(!isFlush);
        }

        @Test
        @DisplayName("Test that the value of a hand is calculated correctly")
        void testNegativeCardsValue() {
            // Arrange
            PlayingCard[] hand = {
                new PlayingCard('H', 1),
                new PlayingCard('H', 2),
                new PlayingCard('H', 3),
                new PlayingCard('H', 4),
                new PlayingCard('H', 5)
            };
            // Act
            int value = CheckHand.cardsValue(hand);
            // Assert
            assert(value != 16);
        }

        @Test
        @DisplayName("Test that the hearts on a hand is returned correctly")
        void testNegativeHeartsOnHand() {
            // Arrange
            PlayingCard[] hand = {
                    new PlayingCard('H', 1),
                    new PlayingCard('S', 12),
                    new PlayingCard('S', 7),
                    new PlayingCard('H', 4),
                    new PlayingCard('C', 9),
                    new PlayingCard('S', 3),
                    new PlayingCard('H', 9),
                    new PlayingCard('S', 10),
                    new PlayingCard('H', 6)
            };
            // Act
            String hearts = CheckHand.heartsOnHand(hand);
            // Assert
            assert(!hearts.equals("H1, H4, H9"));
        }
    }
}
