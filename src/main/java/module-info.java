module edu.ntnu.cardgame {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires java.datatransfer; // Add this line if your code depends on java.datatransfer

    exports edu.ntnu.cardgame.Frondtend to javafx.graphics;

    opens edu.ntnu.cardgame to javafx.fxml;
}