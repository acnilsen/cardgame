package edu.ntnu.cardgame.Frondtend;

import edu.ntnu.cardgame.PlayingCard;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.scene.Group;
import edu.ntnu.cardgame.DeckOfCards;

import java.awt.*;

/**
 * The main class for the card game
 */


public class CardGame extends Application {

    private DeckOfCards deck;
    private PlayingCard[] hand;


    public static void main(String[] args) {
        launch(args);
    }

    /**
     * The start method for the card game
     * @param primaryStage The primary stage
     */

    @Override
    public void start(Stage primaryStage) {
        deck = new DeckOfCards();

        /**
         * The root of the scene
         */

        Group root = new Group();
        Scene scene = new Scene(root, 1200, 700, Color.PEACHPUFF);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Card Game");
        primaryStage.setResizable(false);

        // Set the application icon
        Image icon = new Image("/CardsLogo.png");
        primaryStage.getIcons().add(icon);

        var appIcon = new Image("/CardsLogo.png");
        primaryStage.getIcons().add(appIcon);

        if (Taskbar.isTaskbarSupported()) {
            var taskbar = Taskbar.getTaskbar();

            if (taskbar.isSupported(Taskbar.Feature.ICON_IMAGE)) {
                final Toolkit defaultToolkit = Toolkit.getDefaultToolkit();
                var dockIcon = defaultToolkit.getImage(getClass().getResource("/CardsLogo.png"));taskbar.setIconImage(dockIcon);
            }
        }

        // Create a scroll pane
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefSize(1100, 400);
        scrollPane.setLayoutX((scene.getWidth() - 1100) / 2);
        scrollPane.setLayoutY(20);
        scrollPane.setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        // Create a HBox to hold the cards
        HBox cardWindow = new HBox();
        cardWindow.setPrefSize(1100, 350);
        cardWindow.setLayoutX((scene.getWidth() - 1100) / 2);
        cardWindow.setLayoutY(20);
        cardWindow.setStyle("-fx-background-color: #ffffff;");

        // Add the HBox to the scroll pane
        scrollPane.setContent(cardWindow);

        // Create a text field for the number of cards
        TextField noOfCards = new TextField();
        noOfCards.setPromptText("Enter number of cards");
        noOfCards.setLayoutX(500);
        noOfCards.setLayoutY(500);

        // deal hand based on the number of cards entered
        noOfCards.setOnAction(e -> {
            String text = noOfCards.getText();
            int numberOfCards = Integer.parseInt(text);
            try {
                hand = deck.dealHand(numberOfCards);
                cardWindow.getChildren().clear(); //remove all cards from the card window

                // Add the cards to the card window
                for (PlayingCard card : hand) {
                    Image image = new Image(DeckOfCards.generateImagePath(card.getSuit(), card.getFace()));
                    ImageView imageView = new ImageView(image);
                    imageView.setFitWidth(150);
                    imageView.setPreserveRatio(true);
                    cardWindow.getChildren().add(imageView);
                    imageView.toFront();
                }

                // Remove all labels from the root
                root.getChildren().removeIf(node -> node instanceof Label); // Add this line

                int x = 500;
                int y = 525;
                int counter = 0;

                for (PlayingCard card : hand) {
                    Label handLabel = new Label(card.getAsString());
                    handLabel.getStyleClass().add("handLabel");
                    handLabel.setLayoutX(x);
                    handLabel.setLayoutY(y);
                    root.getChildren().add(handLabel);

                    // disburse the cardLabels in columns of 8
                    counter++;
                    if (counter == 8) {
                        // Reset counter and move to next column
                        counter = 0;
                        x += 100; // Adjust this value to increase or decrease the space between columns
                        y = 525; // Reset y-coordinate for the new column
                    } else {
                        y += 20; // Move to next row in the current column
                    }
                }

            } catch (IllegalArgumentException ex) {
                root.getChildren().removeIf(node -> node instanceof Label);
                Label errorLabel = new Label("Invalid number of cards: " + noOfCards.getText()
                        + ". Must be between 5 and 52");
                errorLabel.setLayoutX(500);
                errorLabel.setLayoutY(550);
                root.getChildren().add(errorLabel);
            }
        });

        Button checkForFlush = getCheckForFlush(noOfCards, root);

        Button checkForValue = getCheckForValue(noOfCards, root);

        Button checkForHearts = getCheckForHearts(noOfCards, root);

        Button checkForSpadesQueen = getCheckForSpadesQueen(noOfCards, root);

        primaryStage.show();

        root.getChildren().add(scrollPane);
        root.getChildren().add(cardWindow);
        root.getChildren().add(noOfCards);
        root.getChildren().add(checkForFlush);
        root.getChildren().add(checkForValue);
        root.getChildren().add(checkForHearts);
        root.getChildren().add(checkForSpadesQueen);
    }

    /**
     * Get the button for checking for flush
     * @param noOfCards The text field for the number of cards
     * @param root The root of the scene
     * @return The button for checking for flush
     */

    private Button getCheckForHearts(TextField noOfCards, Group root) {
        Button checkForHearts = new Button("Check for hearts");
        checkForHearts.setLayoutX(200);
        checkForHearts.setLayoutY(600);
        checkForHearts.setOnAction(e -> {
            String text = noOfCards.getText();
            int numberOfCards = Integer.parseInt(text);
            String hearts = edu.ntnu.cardgame.CheckHand.heartsOnHand(hand);
            root.getChildren().removeIf(node -> node instanceof Label);
            Label heartsLabel = new Label("Hearts: " + hearts);
            heartsLabel.setLayoutX(200);
            heartsLabel.setLayoutY(625);
            root.getChildren().add(heartsLabel);
        });
        return checkForHearts;
    }

    /**
     * Get the button for checking for value
     * @param noOfCards The text field for the number of cards
     * @param root The root of the scene
     * @return The button for checking for value
     */

    private Button getCheckForValue(TextField noOfCards, Group root) {
        Button checkForValue = new Button("Check for value");
        checkForValue.setLayoutX(200);
        checkForValue.setLayoutY(550);
        checkForValue.setOnAction(e -> {
            String text = noOfCards.getText();
            int numberOfCards = Integer.parseInt(text);
            PlayingCard[] hand = deck.dealHand(numberOfCards);
            int value = edu.ntnu.cardgame.CheckHand.cardsValue(hand);
            root.getChildren().removeIf(node -> node instanceof Label);
            Label valueLabel = new Label("Value: " + value);
            valueLabel.setLayoutX(200);
            valueLabel.setLayoutY(575);
            root.getChildren().add(valueLabel);
        });
        return checkForValue;
    }

    /**
     * Get the button for checking for flush
     * @param noOfCards The text field for the number of cards
     * @param root The root of the scene
     * @return The button for checking for flush
     */

    private Button getCheckForFlush(TextField noOfCards, Group root) {
        Button checkForFlush = new Button("Check for flush");
        checkForFlush.setLayoutX(200);
        checkForFlush.setLayoutY(500);
        checkForFlush.setOnAction(e -> {
            String text = noOfCards.getText();
            int numberOfCards = Integer.parseInt(text);
            PlayingCard[] hand = deck.dealHand(numberOfCards);
            if (edu.ntnu.cardgame.CheckHand.isFlush(hand)) {
                root.getChildren().removeIf(node -> node instanceof Label);
                Label flushLabel = new Label("Flush!");
                flushLabel.setLayoutX(200);
                flushLabel.setLayoutY(525);
                root.getChildren().add(flushLabel);
            } else {
                root.getChildren().removeIf(node -> node instanceof Label);
                Label noFlushLabel = new Label("No flush!");
                noFlushLabel.setLayoutX(200);
                noFlushLabel.setLayoutY(525);
                root.getChildren().add(noFlushLabel);
            }
        });
        return checkForFlush;
    }

    private Button getCheckForSpadesQueen(TextField noOfCards, Group root) {
        Button checkForSpadesQueen = new Button("Check for Spades Queen");
        checkForSpadesQueen.setLayoutX(200);
        checkForSpadesQueen.setLayoutY(650);
        checkForSpadesQueen.setOnAction(e -> {
            if (edu.ntnu.cardgame.CheckHand.hasSpadesQueen(hand)) {
                root.getChildren().removeIf(node -> node instanceof Label);
                Label spadesQueenLabel = new Label("Spades Queen!");
                spadesQueenLabel.setLayoutX(200);
                spadesQueenLabel.setLayoutY(675);
                root.getChildren().add(spadesQueenLabel);
            } else {
                root.getChildren().removeIf(node -> node instanceof Label);
                Label noSpadesQueenLabel = new Label("No Spades Queen!");
                noSpadesQueenLabel.setLayoutX(200);
                noSpadesQueenLabel.setLayoutY(675);
                root.getChildren().add(noSpadesQueenLabel);
            }
        });
        return checkForSpadesQueen;
    }

}

