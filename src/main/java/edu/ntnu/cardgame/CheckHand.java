package edu.ntnu.cardgame;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class CheckHand {
    /**
     * Checks if a hand of cards is a flush
     * @param hand The hand of cards
     * @return True if the hand is a flush, false otherwise
     */

    public static boolean isFlush(PlayingCard[] hand) {
        Map<Character, Long> suitCount = Arrays.stream(hand)
                .collect(java.util.stream.Collectors.groupingBy(PlayingCard::getSuit,
                        java.util.stream.Collectors.counting()));

        return suitCount.values().stream().anyMatch(count -> count >= 5);
    }

    /**
     * Calculates the value of a hand of cards
     * @param hand The hand of cards
     * @return The value of the hand
     */

    public static int cardsValue(PlayingCard[] hand) {
        int value = 0;
        for (PlayingCard card : hand) {
            value += card.getFace();
        }
        return value;
    }

    /**
     * Returns the cards in the hand that are hearts
     * @param hand The hand of cards
     * @return A string with the cards that are hearts
     */

    public static String heartsOnHand(PlayingCard[] hand) {

        return Arrays.stream(hand)
                .filter(card -> card.getSuit() == 'H')
                .map(PlayingCard::toString)  // Assuming PlayingCard has a toString method
                .collect(Collectors.joining(", "));
    }

    /**
     * Checks if a hand of cards contains the queen of spades
     * @param hand The hand of cards
     * @return True if the hand contains the queen of spades, false otherwise
     */

    public static Boolean hasSpadesQueen(PlayingCard[] hand) {
        return Arrays.stream(hand)
                .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
    }
}
