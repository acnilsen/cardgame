package edu.ntnu.cardgame;

import java.io.File;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Random;

/**
 * A class representing a deck of cards
 */

public class DeckOfCards {
    private final char[] suits = {'S', 'H', 'D', 'C'};
    private final int[] ranks = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    private final PlayingCard[] deck;

    /**
     * Creates a new deck of cards
     */

    public DeckOfCards() {
        deck = new PlayingCard[52];
        int i = 0;

        for (char suit : suits) {
            for (int rank : ranks) {
                deck[i] = new PlayingCard(suit, rank);
                i++;
            }
        }
    }

    /**
     * Returns the deck of cards
     * @return The deck of cards
     */

    public PlayingCard[] getDeck() {
        return deck;
    }


    /**
     * Generates the url of the image of the card.
     * Created with help from GitHub Copilot Chat
     *
     * @param suit The suit of the card
     * @param rank The rank of the card
     * @return The url of the image of the card
     */
    public static String generateImagePath(char suit, int rank) {
        File file = new File("src/main/resources/cards/" + rank + suit + ".png");
        String url = "";
        try {
            url = file.toURI().toURL().toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return url;
    }

    /**
     * Deals a hand of cards
     * @param n The number of cards to deal
     * @return The hand of cards
     */

    private final Random rand = new Random();

    public PlayingCard[] dealHand(int n) {
        if (n < 5 || n > 52) {
            throw new IllegalArgumentException("Invalid number of cards: " + n + ". Must be between 5 and 52");
        }

        PlayingCard[] handOfCards = new PlayingCard[n];

        while (n > 0) {
            int index = rand.nextInt(52);
            if (deck[index] != null) {
                handOfCards[n - 1] = deck[index];
                deck[index] = null;
                n--;
            }
        }

        return handOfCards;
    }
}

